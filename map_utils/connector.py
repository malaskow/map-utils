"""https://wiki.openstreetmap.org/wiki/API_v0.6"""

_DEFAULT_BASE_URL = "https://api.openstreetmap.org/"


class Connector:
    """Connect to map API."""
    def __init__(self, credentials):
        self._credentials = credentials

    def connect(self):
        """Connect to API."""

    def get(self):
        """GET request."""

    def save(self):
        """save data."""


class HandleRedisDB:
    """Redis handler."""
    def __init__(self, url, credentials):
        """

        :param url:
        :param credentials:
        """
        self._url = url
        self._cred = credentials

    def _connect(self):
        """Connect to DB.

        :return: connection handler.
        """
        pass

    def _get_item(self):
        """Get item."""
        pass

    def save_data(self):
        """Save data."""

    def search_key(self, key):
        """Search key in DB."""
