"""Data gatherer module."""


class DataGatherer:
    """Data Gatherer."""
    def __init__(self, data_list, source):
        """

        :param data_list:
        :param source:
        """
        self._data = self._glue_data(data_list, source)

    @staticmethod
    def _glue_data(data_list, source):
        """

        :param data_list:
        :param source:
        :return:
        """
        return {source: data_list}

    @property
    def data(self):
        """

        :return:
        """
        return self._data

    def make_list(self):
        """Glue all."""
