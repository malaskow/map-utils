# -*- coding: utf-8 -*-
from setuptools import setup


with open('requirements.txt', 'r') as f:
    requirements = f.read().splitlines()


setup(name='map_utils',
      version='0.0.1',
      description='',
      url='',
      author='malaskow',
      author_email='',
      license='??',
      packages=['map_utils'],
      zip_safe=False,
      test_suite='nose.collector',
      tests_require=['nose', 'rednose', 'nose-timer', 'coverage'],
      install_requires=requirements)
